class tilestream::install inherits tilestream {
  include 'epel'

  class { 'nodejs':
    node_pkg    => 'nodejs',
    npm_pkg     => 'npm',
    dev_pkg     => 'nodejs-devel',
    dev_package => true,
    require     => Class['epel'],
  }

  vcsrepo { $tilestream_home:
    ensure   => present,
    provider => git,
    source   => "$git_url",
    require => Class['nodejs'],
  }

  exec { "npm_install":
    command => "npm install",
    cwd => $tilestream_home,
    environment => ["HOME=/home/$tilestream_user"],
    user => 'root',
    require => Vcsrepo[$tilestream_home],
    path => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
  }

  file { 'config.json':
    ensure  => file,
    content => template('tilestream/config.json.erb'),
    path    => "$tilestream_home/config.json",
    require => Vcsrepo[$tilestream_home],
  }

  file { "$tiles":
    ensure  => directory,
    require => File['config.json'],
  }

  package { 'forever':
    ensure   => present,
    provider => 'npm',
    require => File["$tiles"],
  }

  file { '/etc/init.d/tilestream':
    ensure  => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    content => template('tilestream/tilestream.erb'),
    require => Package['forever'],
  }

}
