class tilestream (
  $host = $tilestream::params::host,
  $tileHost = $tilestream::params::tileHost,
  $tilePort = $tilestream::params::tilePort,
  $uiPort = $tilestream::params::uiPort,
  $tiles = $tilestream::params::tiles,
  $tilestream_home = $tilestream::params::tilestream_home,
  $git_url = $tilestream::params::git_url,
  $tilestream_user = $tilestream::params::tilestream_user
) inherits tilestream::params {
  include tilestream::install, tilestream::service
}
