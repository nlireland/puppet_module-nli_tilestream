class tilestream::service inherits tilestream {
  service { 'tilestream':
    ensure => running,
    enable => true,
    hasrestart => true,
    hasstatus => true,
    require => File['/etc/init.d/tilestream'],
  }
}
