# README #

A puppet module to install and configure a Tilestream server. See https://github.com/mapbox/tilestream


## Usage ##

```
#!ruby
# Add the following to your node definition in site.pp

# install the tilestream server without any maps
include tilestream

# load specific maps
# When present, the module will will try to load an mbtiles file for each map in the supplied array from the /files folder.
# E.g. in this case, it will look for parish_map.mbtiles and diocese_map.mbtiles in the /files folder of this module
tilestream::map {['parish_map', 'diocese_map']:}
```

### Variables ###
Public variables are defined in manifests/params.pp. See [tilestream documentation](https://github.com/mapbox/tilestream) for more information on what the parameters do.

Parameters can be overwritten inline:

```
#!puppet

class {'tilestream':
  tileHost => '192.33.11.2',
  tilestream_home => '/var/www/tilestream',
}
```

... or in Hiera:


```
#!puppet
tilestream::tileHost:  '192.33.11.2'
tilestream::tilestream_home:  '/var/www/tilestream'

```

### Updating maps ###
To update a map:  

*  Clone the repository  
*  Overwrite the existing *.mbtiles file in the /files folder  
*  Commit change and push to origin  
*  Git pull and run librarian-puppet update on puppet master
*  Run puppet agent --test on the tilserver node

### Adding maps ###
To add a new map:  

*  Clone the repository  
*  Add your new *.mbtiles file in the /files folder  
*  Commit change and push to origin
*  Update node definition in the NLI-puppet git repo, e.g.

```
#!puppet

tilestream::map {['parish_map', 'diocese_map', 'my_new_map']:}
```
*  Git push changes on NLI-puppet to origin
*  Git pull and run librarian-puppet update on puppet master
*  Run puppet agent --test on the tilserver node