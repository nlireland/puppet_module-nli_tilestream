class tilestream::params {
  $host = '127.0.0.1'
  $tileHost = '127.0.0.1'
  $tilePort = 8888
  $uiPort = 8888
  $tiles = '/usr/local/tilestream-tiles'
  $tilestream_home = '/usr/local/tilestream'
  $git_url = 'https://github.com/mapbox/tilestream.git'
  $tilestream_user = 'puppetadmin'
}
