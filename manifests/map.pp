define tilestream::map ($filename = "$title.mbtiles") {
  include tilestream
  include tilestream::params

  file { "$tilestream::tiles/$filename":
    ensure  => present,
    source  => "puppet:///modules/tilestream/$filename",
    require => File["$tilestream::tiles"],
    notify  => Service["tilestream"],
  }

}
